﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WelcomeSlider : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public float screenWidth = 720;
    public RectTransform[] screens;
    public GameObject[] paginations;
    public float moveSpeed = 100;
    float ScreenRatio { get { return screenWidth / Screen.width; } }


    public float currentDrag = 0;
    public int currentScreen;

    public void OnDrag(PointerEventData eventData)
    {
        currentDrag = Mathf.Clamp(currentDrag + eventData.delta.x * ScreenRatio, -700, 700);
        if (currentScreen == 0 && currentDrag > 200)
        {
            currentDrag = 200;
        }
        else if (currentScreen == screens.Length - 1 && currentDrag < -200)
        {
            currentDrag = -200;
        }


        for (int i = 0; i < screens.Length; i++)
        {
            screens[i].anchoredPosition = new Vector2(screenWidth * (i - currentScreen) + currentDrag, screens[i].anchoredPosition.y);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(currentDrag) > screenWidth * 0.15f)
        {
            currentScreen = Mathf.Clamp(currentScreen + (currentDrag < 0 ? 1 : -1), 0, screens.Length - 1);
        }

        currentDrag = 0;

        for(int i = 0; i < paginations.Length; i++)
        {
            paginations[i].SetActive(i == currentScreen);
        }
    }


    private void Update()
    {
        if (currentDrag == 0)
        {
            for (int i = 0; i < screens.Length; i++)
            {
                screens[i].anchoredPosition = Vector2.MoveTowards(screens[i].anchoredPosition, new Vector2(screenWidth * (i - currentScreen), screens[i].anchoredPosition.y), Time.deltaTime * moveSpeed);

            }
        }
    }
}
