﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string phrase;
    public string postfix;
    
    static Dictionary<string, string> data;


    void Start ()
    {
        Text text = GetComponent<Text>();
        if(text)
        {
            text.text = GetPhrase(phrase) + postfix;
        }
    }
	
	// Update is called once per frame
	public static string GetPhrase(string key)
    {
		if(data == null)
        {
            Localizations loc = JsonUtility.FromJson<Localizations>(Resources.Load<TextAsset>("langs").text);
            string lang = "";
            if (Application.systemLanguage == SystemLanguage.Russian)
            {
                lang = "ru";
            }
            else if (Application.systemLanguage == SystemLanguage.English)
            {
                lang = "en";
            }
            else
            {
                lang = "kk";
            }
            Debug.Log("Current lang " + lang);
            data = new Dictionary<string, string>();
            foreach(LocalizedPhrase locPhrase in loc.data)
            {
                if(lang == "ru")
                {
                    data.Add(locPhrase.key, locPhrase.ru);
                }
                else if(lang == "kk")
                {
                    data.Add(locPhrase.key, locPhrase.kk);
                }
                else
                {
                    data.Add(locPhrase.key, locPhrase.en);
                }
            }
        }

        if(data.ContainsKey(key))
        {
            return data[key];
        }

        return key;
	}
}

[System.Serializable]
public class Localizations
{
    public LocalizedPhrase[] data;
}


[System.Serializable]
public class LocalizedPhrase
{
    public string key;
    public string kk;
    public string ru;
    public string en;
}

